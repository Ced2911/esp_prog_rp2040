// SPDX-License-Identifier: MIT
/*
 * Copyright 2021 Álvaro Fernández Rojas <noltari@gmail.com>
 */
#include <stdint.h>
#include <hardware/irq.h>
#include <hardware/structs/sio.h>
#include <hardware/uart.h>
#include <pico/multicore.h>
#include <pico/stdlib.h>
#include <string.h>
#include <tusb.h>
#include "ch341_device.h"
#include "ws2812.pio.h"

#if !defined(MIN)
#define MIN(a, b) ((a > b) ? b : a)
#endif /* MIN */

#define EN_PIN (18)
#define I0_PIN (19)
#define BTN_PIN (22)

#define ALARM_NUM 1
#define ALARM_IRQ TIMER_IRQ_1
#define ALARM_FREQ 1250

#define LED_PIN 25

#define BUFFER_SIZE 2560

#define DEF_BIT_RATE 115200
#define DEF_STOP_BITS 1
#define DEF_PARITY 0
#define DEF_DATA_BITS 8

typedef struct
{
	uart_inst_t *const inst;
	uint irq;
	void *irq_fn;
	uint8_t tx_pin;
	uint8_t rx_pin;
} uart_id_t;

typedef struct
{
	ch341_line_coding_t usb_lc;
	cdc_line_coding_t uart_lc;
	mutex_t lc_mtx;
	uint8_t uart_buffer[BUFFER_SIZE];
	uint32_t uart_pos;
	mutex_t uart_mtx;
	uint8_t usb_buffer[BUFFER_SIZE];
	uint32_t usb_pos;
	mutex_t usb_mtx;
} uart_data_t;

void uart0_irq_fn(void);
void uart1_irq_fn(void);

const uart_id_t UART_ID[CFG_TUD_CDC] = {
	{
		.inst = uart0,
		.irq = UART0_IRQ,
		.irq_fn = &uart0_irq_fn,
		.tx_pin = 16,
		.rx_pin = 17,
	}};

uart_data_t UART_DATA[CFG_TUD_CDC];

static inline uint databits_usb2uart(uint8_t data_bits)
{
	switch (data_bits)
	{
	case 5:
		return 5;
	case 6:
		return 6;
	case 7:
		return 7;
	default:
		return 8;
	}
}

static inline uart_parity_t parity_usb2uart(uint8_t usb_parity)
{
	switch (usb_parity)
	{
	case 1:
		return UART_PARITY_ODD;
	case 2:
		return UART_PARITY_EVEN;
	default:
		return UART_PARITY_NONE;
	}
}

static inline uint stopbits_usb2uart(uint8_t stop_bits)
{
	switch (stop_bits)
	{
	case 2:
		return 2;
	default:
		return 1;
	}
}

void update_uart_cfg(uint8_t itf)
{
	const uart_id_t *ui = &UART_ID[itf];
	uart_data_t *ud = &UART_DATA[itf];

	mutex_enter_blocking(&ud->lc_mtx);

	if (ud->usb_lc.bit_rate != ud->uart_lc.bit_rate)
	{
		uart_set_baudrate(ui->inst, ud->usb_lc.bit_rate);
		ud->uart_lc.bit_rate = ud->usb_lc.bit_rate;
	}

	if ((ud->usb_lc.stop_bits != ud->uart_lc.stop_bits) ||
		(ud->usb_lc.parity != ud->uart_lc.parity) ||
		(ud->usb_lc.data_bits != ud->uart_lc.data_bits))
	{
		uart_set_format(ui->inst,
						databits_usb2uart(ud->usb_lc.data_bits),
						stopbits_usb2uart(ud->usb_lc.stop_bits),
						parity_usb2uart(ud->usb_lc.parity));
		ud->uart_lc.data_bits = ud->usb_lc.data_bits;
		ud->uart_lc.parity = ud->usb_lc.parity;
		ud->uart_lc.stop_bits = ud->usb_lc.stop_bits;
	}

	mutex_exit(&ud->lc_mtx);
}

void usb_read_bytes(uint8_t itf)
{
	tud_task();

	uart_data_t *ud = &UART_DATA[itf];
	uint32_t len = MIN(tud_ch341_available(), BUFFER_SIZE - ud->usb_pos);

	if (len)
	{
		uint32_t count = tud_ch341_read(&ud->usb_buffer[ud->usb_pos], len);
		ud->usb_pos += count;
	}
}

void usb_write_bytes(uint8_t itf)
{
	tud_task();
	uart_data_t *ud = &UART_DATA[itf];

	if (
		tud_ch341_connected() &&
		ud->uart_pos)
	{
		uint32_t count = tud_ch341_write(ud->uart_buffer, ud->uart_pos);
		if (count < ud->uart_pos)
			memcpy(ud->uart_buffer, &ud->uart_buffer[count],
				   ud->uart_pos - count);
		ud->uart_pos -= count;

		tud_task();

		tud_ch341_write_flush();
	}
	tud_task();
}

void usb_cdc_process(uint8_t itf)
{
	uart_data_t *ud = &UART_DATA[itf];

	mutex_enter_blocking(&ud->lc_mtx);
	tud_ch341_get_line_coding(&ud->usb_lc);
	mutex_exit(&ud->lc_mtx);

	usb_read_bytes(itf);
	usb_write_bytes(itf);
}

static inline size_t uart_read_bytes(uint8_t itf)
{
	size_t l = 0;
	uart_data_t *ud = &UART_DATA[itf];
	const uart_id_t *ui = &UART_ID[itf];

	if (uart_is_readable(ui->inst))
	{
		while (uart_is_readable(ui->inst) &&
			   (ud->uart_pos < BUFFER_SIZE))
		{
			ud->uart_buffer[ud->uart_pos] = uart_get_hw(ui->inst)->dr;
			ud->uart_pos++;
			l++;
		}
	}
	return l;
}

static size_t uart_read_bloc(uint8_t itf, uint8_t *data, size_t len)
{
	size_t l = 0;
	const uart_id_t *ui = &UART_ID[itf];
	while (uart_is_readable(ui->inst) && l < len)
	{
		data[l++] = uart_get_hw(ui->inst)->dr;
	}
	return l;
}

static size_t usb_read_bloc(uint8_t itf, uint8_t *data, size_t len)
{
	tud_task();
	if (tud_ch341_available())
	{
		tud_task();
		size_t l = tud_ch341_read(data, len);
		return l;
	}
	return 0;
}

void uart0_irq_fn(void)
{
	uart_read_bytes(0);
}

void uart1_irq_fn(void)
{
	uart_read_bytes(1);
}

size_t uart_write_bytes(uint8_t itf)
{
	size_t l = 0;
	uart_data_t *ud = &UART_DATA[itf];

	if (ud->usb_pos)
	{
		const uart_id_t *ui = &UART_ID[itf];
		uint32_t count = 0;

		while (uart_is_writable(ui->inst) &&
			   count < ud->usb_pos)
		{
			// uart_putc_raw(ui->inst, ud->usb_buffer[count]);
			uart_get_hw(ui->inst)->dr = ud->usb_buffer[count];
			count++;
			l++;
		}

		if (count < ud->usb_pos)
			memcpy(ud->usb_buffer, &ud->usb_buffer[count],
				   ud->usb_pos - count);
		ud->usb_pos -= count;
	}
	return l;
}

void init_uart_data(uint8_t itf)
{
	const uart_id_t *ui = &UART_ID[itf];
	uart_data_t *ud = &UART_DATA[itf];

	/* Pinmux */
	gpio_set_function(ui->tx_pin, GPIO_FUNC_UART);
	gpio_set_function(ui->rx_pin, GPIO_FUNC_UART);

	/* USB CDC LC */
	ud->usb_lc.bit_rate = DEF_BIT_RATE;
	ud->usb_lc.data_bits = DEF_DATA_BITS;
	ud->usb_lc.parity = DEF_PARITY;
	ud->usb_lc.stop_bits = DEF_STOP_BITS;

	/* UART LC */
	ud->uart_lc.bit_rate = DEF_BIT_RATE;
	ud->uart_lc.data_bits = DEF_DATA_BITS;
	ud->uart_lc.parity = DEF_PARITY;
	ud->uart_lc.stop_bits = DEF_STOP_BITS;

	/* Buffer */
	ud->uart_pos = 0;
	ud->usb_pos = 0;

	/* Mutex */
	mutex_init(&ud->lc_mtx);
	mutex_init(&ud->uart_mtx);
	mutex_init(&ud->usb_mtx);

	/* UART start */
	uart_init(ui->inst, ud->usb_lc.bit_rate);
	uart_set_hw_flow(ui->inst, false, false);
	// uart_set_hw_flow(ui->inst, true, true);
	uart_set_format(ui->inst, databits_usb2uart(ud->usb_lc.data_bits),
					stopbits_usb2uart(ud->usb_lc.stop_bits),
					parity_usb2uart(ud->usb_lc.parity));
	/* UART RX Interrupt */
	// uart_set_fifo_enabled(ui->inst, false);
	/*

	irq_set_exclusive_handler(ui->irq, ui->irq_fn);
	irq_set_enabled(ui->irq, true);
	uart_set_irq_enables(ui->inst, true, false);
	*/
}

void uart_task_init()
{
	tusb_init();
}

void uart_task_loop()
{
	int itf;

	// tud_task();
	for (itf = 0; itf < CFG_TUD_CDC; itf++)
	{
		uart_data_t *ud = &UART_DATA[itf];
		const uart_id_t *ui = &UART_ID[itf];
		update_uart_cfg(itf);
		tud_ch341_get_line_coding(&ud->usb_lc);
		/*
		uart_read_bytes(itf);
		usb_write_bytes(itf);
		usb_read_bytes(itf);
		uart_write_bytes(itf);
		*/

		uint8_t buffer[256];

		// uart => usb
		size_t uart_r_len = uart_read_bloc(itf, buffer, 256);
		if (uart_r_len > 0)
		{
			tud_ch341_write(buffer, uart_r_len);
			tud_ch341_write_flush();
		}

		// usb => uart
		size_t usb_r_len = usb_read_bloc(itf, buffer, 256);
		if (usb_r_len > 0)
		{
			uart_write_blocking(ui->inst, buffer, usb_r_len);
		}
		tud_task();
	}
}

void core_entry_no_irq(void)
{

	uart_task_init();
	while (1)
		uart_task_loop();
}

#define NUM_PIXELS 150
#define IS_RGBW (0)
#define WS2812_PIN (23)
#include "led.h"

static int en_pin_value = 1;
static int i0_pin_value = 0;

static int prog_dtr = 0;
static int prog_rts = 0;

static void apply_cdc_state(bool dtr, bool rts)
{
	/*
		static uint8_t _cdc_state = 0;

		uint8_t cdc_state = (dtr ? 2 : 0) | (rts ? 1 : 0);

		if (cdc_state != _cdc_state)
		{
			switch (cdc_state)
			{
			case 0b00:
			case 0b11:
				en_pin_value = 1;
				i0_pin_value = 1;
				break;
			case 0b10:
				en_pin_value = 0;
				i0_pin_value = 1;
				break;
			case 0b01:
				en_pin_value = 1;
				i0_pin_value = 0;
				break;
			}
			// printf("")
		}

		_cdc_state = cdc_state;
	*/
	prog_rts = rts;
	prog_dtr = dtr;

	en_pin_value = rts;
	i0_pin_value = dtr;
}

void tud_cdc_line_state_cb(uint8_t itf, bool dtr, bool rts)
{
	apply_cdc_state(dtr, rts);
}

void tud_ch341_line_state_cb(ch341_line_state_t line_state)
{
	apply_cdc_state(line_state & CH341_LINE_STATE_DTR_ACTIVE ? 1 : 0, line_state & CH341_LINE_STATE_RTS_ACTIVE ? 1 : 0);
}

void tud_ch341_line_coding_cb(ch341_line_coding_t const *p_line_coding)
{
}

unsigned long btn_debouncer_time = 0;
const int btn_debouncer_delay = 400; // Delay for every push button may vary

enum
{
	PROG_MODE_BOOTLOADER,
	PROG_MODE_RELEASE,
	PROG_MODE_AUTO
};
static int state_prog_mode = PROG_MODE_AUTO;

void prog_btn_intr(uint gpio, uint32_t events)
{
	if ((to_ms_since_boot(get_absolute_time()) - btn_debouncer_time) > btn_debouncer_delay)
	{
		btn_debouncer_time = to_ms_since_boot(get_absolute_time());

		switch (state_prog_mode)
		{
		case PROG_MODE_BOOTLOADER:
			state_prog_mode = PROG_MODE_RELEASE;
			break;
		case PROG_MODE_RELEASE:
			state_prog_mode = PROG_MODE_AUTO;
			break;
		case PROG_MODE_AUTO:
			state_prog_mode = PROG_MODE_BOOTLOADER;
			break;
		}
	}
}

void prog_gpio_init()
{
	btn_debouncer_time = to_ms_since_boot(get_absolute_time());

	gpio_init(LED_PIN);
	gpio_set_dir(LED_PIN, GPIO_OUT);

	gpio_init(EN_PIN);
	gpio_init(I0_PIN);
	gpio_init(BTN_PIN);

	gpio_set_dir(EN_PIN, GPIO_OUT);
	gpio_set_dir(I0_PIN, GPIO_OUT);
	gpio_set_dir(BTN_PIN, GPIO_IN);

	//	gpio_set_function(EN_PIN, GPIO_FUNC_NULL);
	//	gpio_set_function(I0_PIN, GPIO_FUNC_NULL);
	// 	gpio_set_function(BTN_PIN, GPIO_FUNC_NULL);

	gpio_pull_up(BTN_PIN);
	gpio_set_irq_enabled_with_callback(BTN_PIN, GPIO_IRQ_EDGE_FALL, true, &prog_btn_intr);
}

struct repeating_timer timer;

bool prog_rgb_led_timer(struct repeating_timer *t)
{
	// put_pixel(urgb_u32(i0_pin_value ? 0xff : 0, en_pin_value ? 0xff : 0, 0));
	switch (state_prog_mode)
	{
	case PROG_MODE_BOOTLOADER:
		put_pixel(urgb_u32(0xff, 0, 0));
		break;
	case PROG_MODE_RELEASE:
		put_pixel(urgb_u32(0, 0xff, 0));
		break;
	case PROG_MODE_AUTO:
		// put_pixel(urgb_u32(i0_pin_value ? 0x3f : 0, en_pin_value ? 0x3f : 0, 0xff));
		put_pixel(urgb_u32(0, prog_rts ? 0 : 0xff, prog_dtr ? 0 : 0xff));
		break;
	}
	return true;
}

void prog_rgb_led_init()
{
	PIO pio = pio0;
	int sm = 0;
	uint offset = pio_add_program(pio, &ws2812_program);

	ws2812_program_init(pio, sm, offset, WS2812_PIN, 800000, IS_RGBW);
	put_pixel(urgb_u32(0xff, 0, 0xff));

	add_repeating_timer_ms(50, prog_rgb_led_timer, NULL, &timer);
}

void prog_reset_sequence(int sequence)
{
	switch (sequence)
	{
	case 0:
		break;
	default:
		break;
	}
}

int old_prog_state = -1;
int old_prog_auto_state = -1;
int prog_state_cnt = 0;
int prog_auto_reset_state = 0;

void prog_task_init()
{
	old_prog_state = -1;
	prog_state_cnt = 0;
	prog_auto_reset_state = 0;
	old_prog_auto_state = -1;
}

static void nop_x9()
{

	asm volatile("nop \n nop \n nop");
	asm volatile("nop \n nop \n nop");
	asm volatile("nop \n nop \n nop");
}

void prog_task_update()
{
	if (state_prog_mode == PROG_MODE_AUTO)
	{
		if (old_prog_state != PROG_MODE_AUTO)
		{
			// reset io values
			i0_pin_value = 1;
			en_pin_value = 1;
			prog_auto_reset_state = 0;
		}

		// rts => vert
		// dtr => bleu => connecté
		uint8_t prog_auto_state = (prog_dtr ? 2 : 0) | (prog_rts ? 1 : 0);
		if (old_prog_auto_state != prog_auto_state)
		{
			switch (prog_auto_state)
			{
			case 0b00:
			case 0b11:
				// stop
				gpio_put(I0_PIN, 0);
				nop_x9();
				gpio_put(EN_PIN, 0);
				nop_x9();

				// release mode
				en_pin_value = 1;
				i0_pin_value = 1;
				gpio_put(I0_PIN, i0_pin_value);
				nop_x9();
				gpio_put(EN_PIN, en_pin_value);
				break;
			case 0b01:
				en_pin_value = 0;
				i0_pin_value = 1;
				gpio_put(I0_PIN, i0_pin_value);
				nop_x9();
				gpio_put(EN_PIN, en_pin_value);
				break;
			case 0b10:
				// bootloader mode
				en_pin_value = 1;
				i0_pin_value = 0;
				gpio_put(I0_PIN, i0_pin_value);
				nop_x9();
				gpio_put(EN_PIN, en_pin_value);
				break;
			}
		}

		//
		old_prog_state = state_prog_mode;
		old_prog_auto_state = prog_auto_state;
		prog_state_cnt = 0;
	}
	else if (state_prog_mode != old_prog_state)
	{
		switch (prog_state_cnt)
		{
		case (0):
			// disable en
			gpio_put(EN_PIN, 0);
			// next step
			prog_state_cnt++;
			break;
		case (1 << 8):
			switch (state_prog_mode)
			{
			case PROG_MODE_BOOTLOADER:
				gpio_put(I0_PIN, 0);
				break;
			case PROG_MODE_RELEASE:
				gpio_put(I0_PIN, 1);
				break;
			}
			prog_state_cnt++;
			break;
		case (1 << 9):
			// enable en
			gpio_put(EN_PIN, 1);
			// finished
			prog_state_cnt = 0;
			old_prog_state = state_prog_mode;
			break;
		default:
			prog_state_cnt++;
		}
	}
}

int main(void)
{
	int itf;

	set_sys_clock_khz(250000, false);

	// usbd_serial_init();

	for (itf = 0; itf < CFG_TUD_CDC; itf++)
		init_uart_data(itf);

	prog_gpio_init();
	prog_rgb_led_init();

	multicore_launch_core1(core_entry_no_irq);

	sleep_ms(500);

	prog_task_init();

	while (1)
	{
		prog_task_update();
		sleep_us(1);
	}
	return 0;
}
