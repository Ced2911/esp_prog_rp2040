// SPDX-License-Identifier: MIT
/*
 * Copyright (c) 2021 Álvaro Fernández Rojas <noltari@gmail.com>
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 * Copyright (c) 2020 Damien P. George
 */

#if !defined(_TUSB_CONFIG_H_)
#define _TUSB_CONFIG_H_

#include <tusb_option.h>

#ifndef CFG_TUSB_RHPORT0_MODE
#define CFG_TUSB_RHPORT0_MODE OPT_MODE_DEVICE | OPT_MODE_FULL_SPEED
#endif

#ifndef BOARD_TUD_RHPORT
#define BOARD_TUD_RHPORT      0
#endif


#define CFG_TUD_CDC 1
#define CFG_TUD_CDC_RX_BUFSIZE 256
#define CFG_TUD_CDC_TX_BUFSIZE 256

// Enable Device stack
//#define CFG_TUD_ENABLED       1

#ifndef CFG_TUD_ENDPOINT0_SIZE
#define CFG_TUD_ENDPOINT0_SIZE    64
#endif

// CH341 Endpoint max packet sizes (should not change)
#define CFG_TUD_CH341_EP_RX_MAX_PACKET   (TUD_OPT_HIGH_SPEED ? 512 : 64)
#define CFG_TUD_CH341_EP_TX_MAX_PACKET   CFG_TUD_CH341_EP_RX_MAX_PACKET
#define CFG_TUD_CH341_EP_TXNOTIFY_MAX_PACKET  (8)

// CH341 buffer size for TX and RX data (must be an interval of max packet size)
// more is faster
#define CFG_TUD_CH341_FIFO_SIZE   (TUD_OPT_HIGH_SPEED ? 512 : 64)

void usbd_serial_init(void);

#endif /* _TUSB_CONFIG_H_ */
